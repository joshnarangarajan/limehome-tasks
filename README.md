# Limehome Tasks

## Task 1

### TEST SCENARIO 1 : Users are allowed to Checkin between 48 hours and 90 minutes before checkin time

Eg Scenario: Check in Date: June 20, 2020 , Check in Time 10:30 and Checkout date is June 22,2020 

#### TestCase1:Users should be able to Checkin from June 18 , 2020 at 10:30 am
Details:
  * Navigate to the online Checkin page
  * Assuming the time is 10:30 am and date is June 18,2020(48 hours ahead of booking time)
  * Enter the Firstname, Lastname, Booking Reference Number
  * Click on Submit
  * User is allowed to check in
  * User gets sms, email with room number and access code
  
#### Test Case2: Users should be able to Checkin on June 20,2020 until  9:00 am
Details:
  * Navigate to the online Checkin page
  * Assuming the time is 09:00 am and date is June 18,2020(before 90 minutes of booking time)
  * Enter the Firstname, Lastname, Booking Reference Number
  * Click on Submit
  * User is allowed to check in
  * User gets sms, email with room number and access code
  
### TEST SCENARIO 2: Users are not allowed to checkin before 48 hours and after 90 minutes of Checkin time

#### Test Case1: Users should not be able to Checkin before 10:30 am on June 18,2020
Details:
  * Navigate to the online Checkin page
  * Assuming the time is 10:25am and date is June 18,2020 (not 48 hours of booking time)
  * Enter the Firstname, Lastname, Booking Reference Number
  * Click on Submit
  * User is not allowed to check in
  * User does not get an email, sms
  
  #### Test Case2: Users should not be able to Checkin after 09:00 am on June 18,2020
Details:
  * Navigate to the online Checkin page
  * Assuming the time is 09:01 am and date is June 18, 2020 ( after 90 minutes of booking time)
  * Enter the Firstname, Lastname, Booking Reference Number
  * Click on Submit
  * User is not allowed to check in
  * User does not get sms, email 
  
### TEST SCENARIO 3: Retrieve a Reservation Details using reference number and guest name

#### Test Case1: Positive Test Case: Users are able to check in when Lastname, Reference number are entered correctly.
Details:
Eg: Lastname = XXXXXX , Reference Number = abcd123456 given during online checkin
  * Navigate to https://limehome-qa-task.herokuapp.com
  * Goto Retrieve a booking page
  * Enter “Lastname =XXXXXX” , “Reference Number= abcd123456” in their respective fields.
  * Click on Submit
  * Navigates to the next page -> Booking Details Page

#### Test Case2: Users are not able to checkin with wrong Lastname and Reference Number
Details:
  * Navigate to https://limehome-qa-task.herokuapp.com
  * Goto Retrieve a booking page
  * Enter wrong “Lastname = YYYYYY” , wrong “Reference Number = 123456xyz” in their respective fields.
  * Click on Submit
  * Error Message should appear
  * Enter wrong “Lastname = YYYYYY” , correct “Reference Number = abcd123456”  in their respective fields.
  * Click on Submit
  * Error Message should appear
  * Enter correct “Lastname = XXXXXX” , wrong “Reference Number = 123456xyz” in their respective fields.
  * Click on Submit
  * Error Message should appear
  
### TEST SCENARIO 4: Passport ID, Passport Picture Upload mandatory for non-german nationalities

#### Test Case1:  Passport  ID, Passport picture mandatory for Non German Nationals

Description: Positive Test Case: Users need to enter Passport ID, Passport Picture if their nationality is not German.

Details: 
  * Goto Retrieve a booking -> Enter Last Name, Reference Number-> Click Submit-> Navigates to next page
  * Enter the Nationality-> Eg: Danish 
  * Users are able to see a mandatory field “Passport ID” and an upload icon 
  * Enter the Passport ID an upload a passport picture
  * Click on Submit
  * Navigates to next Page -> “Thankyou” page

#### Test Case2: Passport ID , Passport picture not required for German Nationals 

Description: Positive Test Case: Users need not enter Passport ID, Passport Picture if their nationality is German.

Details: 
  * Goto Retrieve a booking -> Enter Last Name, Reference Number-> Click Submit-> Navigates to next page
  * Enter the Nationality-> Eg: ”German”
  * No mandatory field pops out when the nationality  “German” is selected.
  * Click on Submit
  * Navigates to next Page -> “Thankyou” page

#### Test Case 3: Validation Error

Description: Negative Test Case:

Details: 
  * Goto Retrieve a booking -> Enter Last Name, Reference Number-> Click Submit-> Navigates to next page
  * Enter the Nationality-> Eg: Danish 
  * Users are able to see a mandatory field “Passport ID” and an upload icon 
  * Do not enter the Passport ID and do not upload a passport picture
  * Click on Submit ->Validation message appears “Please double check the highlighted fields”
  * Enter only Passport ID and do not upload Passport picture
  * Click on Submit ->Validation message appears “Please double check the highlighted fields”
  * Upload only Passport picture and do not enter Passport ID
  * Click on Submit ->Validation message appears “Please double check the highlighted fields”
  
### TEST SCENARIO 5: Users can do online checkin by using Mobile web app

The same Test Cases of the web app can be reused to test the online checkin feature by using Mobile App

### TEST SCENARIO 6: Check the bilingualism of Web / Mobile App

#### Test Case 1: Check the bilingualism of Web App

1a. Description: Positive Test Case:  To check if users can navigate to the English and Deutsch Version of the Online Checkin Page

Details:
  * Navigate to https://limehome-qa-task.herokuapp.com/
  * Goto to the top right corner of the page -> View EN/ DE
  * Click on EN -> Should navigate to English version
  * Click on Back
  * Click on DE-> Should navigate to Deutsch version

1b. Description: Negative Test Case:  To check if users navigate to the wrong version of the online checkin page when English and Deutsch Version selected

Details:
  * Navigate to https://limehome-qa-task.herokuapp.com/
  * Goto to the top right corner of the page -> View EN/ DE
  * Click on DE ->Navigates to Deutsch version
  * Click on Back
  * Click on DE->Navigates to English version

1c. Description: Negative Test Case:  Users are unable to navigate to the online checkin page when English and Deutsch Version selected

Details:
  * Navigate to https://limehome-qa-task.herokuapp.com/
  * Goto to the top right corner of the page -> View EN/ DE
  * Click on EN -> Does not navigate to any page
  * Click on Back
  * Click on DE-> Does not navigate to any page
  
#### Test Case 2: Check the bilingualism of Mobile App 

The test cases for the web app can be reused for testing the bilingualism of mobile app

## Task 2
### Finding Defects (Web App and Mobile App)

Below are some of the defects that are existing in the Application.

  * The field “Last Name“ can be entered with special characters 
  * In the given text, it states that the booking reference code is 10-character alphanumeric code but the booking reference field’s validation message states that the required characters can be minimum 6 characters.
  * Less than 10 alphanumeric characters are accepted in the “booking reference” field.
  * The Booking reference field accepts special characters also which should not be possible.
  * The missing fields are not highlighted when the “Submit” button is clicked on the booking details page even though the validation message says “Please double check the highlightes fields”
  * The “Date of Birth” Field accepts future months also which should not be possible.
  * The German language translation is not working on the Lime home web and mobile application.

## Task 3
### Defect Reporting

Reporting of an existing defect 

File -> New Entry

**Title**      : Missing Fields not Highlighted

**Priority**   : Medium            
**Severity**   : Medium                  
**Type**       : New Functionality           
**Sprint**     : 1    
**Found By**   : XXXXX             
**Assigned**   : YYYYY             
**Screenshot** : Yes                     
**Attachment** : .jpeg 

Steps to Reproduce:

  * Navigate to https://limehome-qa-task.herokuapp.com
  * Goto Retrieve a Booking Page
  * Enter “Last Name”, “Booking Reference” 
  * Click on Submit
  * Enter all the fields except the field “Date of Birth”
  * Click on Submit

